-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-07-2021 a las 06:30:22
-- Versión del servidor: 10.4.19-MariaDB
-- Versión de PHP: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `heladosa_bd_altena`
--
CREATE DATABASE IF NOT EXISTS `heladosa_bd_altena` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `heladosa_bd_altena`;

DELIMITER $$
--
-- Procedimientos
--
DROP PROCEDURE IF EXISTS `borrarOpcionPerfil`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `borrarOpcionPerfil` (IN `ban` INT, IN `cvePerfil_i` INT)  BEGIN
	
	-- 1. Elimina todas las opciones dependiendo el Perfil
	
	CASE
		WHEN ban = 1 THEN
		
			DELETE FROM de_perfilopcion WHERE cveperfil_perfilopcion = cvePerfil_i;
			
		
			
	END CASE;

END$$

DROP PROCEDURE IF EXISTS `eliminarCliente`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminarCliente` (IN `ban` INT, IN `cveCliente_i` INT, IN `cveUsuarioAccion_i` INT)  BEGIN
	
	-- 1. Elimina el cliente de la BD
	-- 2. Cambia el estatus del cliente a 0
	-- 3. Cambia el estatus del cliente a 1
	
	CASE
		WHEN ban = 1 THEN
		
			DELETE FROM ca_clientes WHERE cve_cliente = cveCliente_i;
			
		WHEN ban = 2 THEN
		
			UPDATE ca_clientes 
					SET estatus_cliente = 0,
							cveusuariomod_cliente = cveUsuarioAccion_i,
							fechamod_cliente = NOW()
					WHERE cve_cliente = cveCliente_i;
					
		WHEN ban = 3 THEN
		
			UPDATE ca_clientes 
					SET estatus_cliente = 1,
							cveusuariomod_cliente = cveUsuarioAccion_i,
							fechamod_cliente = NOW()
					WHERE cve_cliente = cveCliente_i;
			
	END CASE;

END$$

DROP PROCEDURE IF EXISTS `eliminarPerfil`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminarPerfil` (IN `ban` INT, IN `cvePerfil_i` INT, IN `cveUsuarioAccion_i` INT)  BEGIN
	
	-- 1. Elimina el perfil de la BD
	-- 2. Cambia el estatus del perfil a 0
	-- 3. Cambia el estatus del perfil a 1
	
	CASE
		WHEN ban = 1 THEN
		
			DELETE FROM ca_perfil WHERE cve_perfil = cvePerfil_i;
			
		WHEN ban = 2 THEN
		
			UPDATE ca_perfil 
					SET estatus_perfil = 0,
							cveusuariomod_perfil = cveUsuarioAccion_i,
							fechamod_perfil = NOW()
					WHERE cve_perfil = cvePerfil_i;
					
		WHEN ban = 3 THEN
		
			UPDATE ca_perfil 
					SET estatus_perfil = 1,
							cveusuariomod_perfil = cveUsuarioAccion_i,
							fechamod_perfil = NOW()
					WHERE cve_perfil = cvePerfil_i;
			
	END CASE;

END$$

DROP PROCEDURE IF EXISTS `eliminarProducto`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminarProducto` (IN `ban` INT, IN `cve_producto_i` INT, IN `cveUsuarioAccion_i` INT)  BEGIN
	
	-- 1. Elimina la producto de la BD
	-- 2. Cambia el estatus de la producto a 0
	-- 3. Cambia el estatus de la producto a 1
	
	CASE
		WHEN ban = 1 THEN
		
			DELETE FROM ca_productos WHERE cve_producto = cve_producto_i;
			
		WHEN ban = 2 THEN
		
			UPDATE ca_productos 
					SET estatus_producto = 0,
							cveusuariomod_producto = cveUsuarioAccion_i,
							fechamod_producto = NOW()
					WHERE cve_producto = cve_producto_i;
					
		WHEN ban = 3 THEN
		
			UPDATE ca_productos 
					SET estatus_producto = 1,
							cveusuariomod_producto = cveUsuarioAccion_i,
							fechamod_producto = NOW()
					WHERE cve_producto = cve_producto_i;
			
	END CASE;

END$$

DROP PROCEDURE IF EXISTS `eliminarPuesto`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminarPuesto` (IN `ban` INT, IN `cvePuesto_i` INT, IN `cveUsuarioAccion_i` INT)  BEGIN
	
	-- 1. Elimina el puesto de la BD
	-- 2. Cambia el estatus del puesto a 0
	-- 3. Cambia el estatus del puesto a 1
	
	CASE
		WHEN ban = 1 THEN
		
			DELETE FROM ca_puestos WHERE cve_puesto = cvePuesto_i;
			
		WHEN ban = 2 THEN
		
			UPDATE ca_puestos 
					SET estatus_puesto = 0,
							cveusuariomod_puesto = cveUsuarioAccion_i,
							fechamod_puesto = NOW()
					WHERE cve_puesto = cvePuesto_i;
					
		WHEN ban = 3 THEN
		
			UPDATE ca_puestos 
					SET estatus_puesto = 1,
							cveusuariomod_puesto = cveUsuarioAccion_i,
							fechamod_puesto = NOW()
					WHERE cve_puesto = cvePuesto_i;
			
	END CASE;

END$$

DROP PROCEDURE IF EXISTS `eliminarSabor`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminarSabor` (IN `ban` INT, IN `cve_sabor_i` INT, IN `cveUsuarioAccion_i` INT)  BEGIN
	
	-- 1. Elimina el sabor de la BD
	-- 2. Cambia el estatus del sabor a 0
	-- 3. Cambia el estatus del sabor a 1
	
	CASE
		WHEN ban = 1 THEN
		
			DELETE FROM ca_sabores WHERE cve_sabor = cve_sabor_i;
			
		WHEN ban = 2 THEN
		
			UPDATE ca_sabores 
					SET estatus_sabor = 0,
							cveusuariomod_sabor = cveUsuarioAccion_i,
							fechamod_sabor = NOW()
					WHERE cve_sabor = cve_sabor_i;
					
		WHEN ban = 3 THEN
		
			UPDATE ca_sabores 
					SET estatus_sabor = 1,
							cveusuariomod_sabor = cveUsuarioAccion_i,
							fechamod_sabor = NOW()
					WHERE cve_sabor = cve_sabor_i;
			
	END CASE;

END$$

DROP PROCEDURE IF EXISTS `eliminarSucursal`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminarSucursal` (IN `ban` INT, IN `cveSucursal_i` INT, IN `cveUsuarioAccion_i` INT)  BEGIN
	
	-- 1. Elimina la sucursal de la BD
	-- 2. Cambia el estatus de la sucursal a 0
	-- 3. Cambia el estatus de la sucursal a 1
	
	CASE
		WHEN ban = 1 THEN
		
			DELETE FROM ca_sucursales WHERE cve_sucursal = cveSucursal_i;
			
		WHEN ban = 2 THEN
		
			UPDATE ca_sucursales 
					SET estatus_sucursal = 0,
							cveusuariomod_sucursal = cveUsuarioAccion_i,
							fechamod_sucursal = NOW()
					WHERE cve_sucursal = cveSucursal_i;
					
		WHEN ban = 3 THEN
		
			UPDATE ca_sucursales 
					SET estatus_sucursal = 1,
							cveusuariomod_sucursal = cveUsuarioAccion_i,
							fechamod_sucursal = NOW()
					WHERE cve_sucursal = cveSucursal_i;
			
	END CASE;

END$$

DROP PROCEDURE IF EXISTS `eliminarUsuario`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminarUsuario` (IN `ban` INT, IN `cveUsuario_i` INT, IN `cveUsuarioAccion_i` INT)  BEGIN
	
	-- 1. Elimina el usuario de la BD
	-- 2. Cambia el estatus del usuario a 0
	-- 3. Cambia el estatus del usuario a 1
	
	CASE
		WHEN ban = 1 THEN
		
			DELETE FROM ca_usuario WHERE cve_usuario = cveUsuario_i;
			
		WHEN ban = 2 THEN
		
			UPDATE ca_usuario 
					SET estatus_usuario = 0,
							cveusuariomod_usuario = cveUsuarioAccion_i,
							fechamod_usuario = NOW()
					WHERE cve_usuario = cveUsuario_i;
					
		WHEN ban = 3 THEN
		
			UPDATE ca_usuario 
					SET estatus_usuario = 1,
							cveusuariomod_usuario = cveUsuarioAccion_i,
							fechamod_usuario = NOW()
					WHERE cve_usuario = cveUsuario_i;
			
	END CASE;

END$$

DROP PROCEDURE IF EXISTS `guardarCliente`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `guardarCliente` (IN `ban` INT, IN `cveCliente_i` INT, IN `nombreCliente_i` VARCHAR(100), IN `direccionCliente_i` VARCHAR(120), IN `comentarioCliente_i` VARCHAR(75), IN `celularCliente_i` VARCHAR(150), IN `cveUsuarioAccion_i` INT)  begin
	
	-- 1. Guarda una cliente y guarda cambios
	
	CASE
		WHEN ban = 1 THEN
		
			IF cveCliente_i < 1 THEN
			
				INSERT INTO ca_clientes  
					(
					
					nombre_cliente,
					celular_cliente,
					direccion_cliente,
					comentario_cliente,
					estatus_cliente,
					cveusuarioadd_cliente,
					fechaadd_cliente
					) VALUES (
										nombreCliente_i,
										celularCliente_i,
										direccionCliente_i,
										comentarioCliente_i,
										1,
										cveUsuarioAccion_i,
										NOW()
									 );
				
			ELSE
			
				UPDATE ca_clientes 
				SET 
						nombre_cliente = nombreCliente_i,
						celular_cliente = celularCliente_i,
						direccion_cliente = direccionCliente_i,
						comentario_cliente = comentarioCliente_i,
						cveusuariomod_cliente = cveUsuarioAccion_i,
						fechamod_cliente = NOW()
				WHERE cve_cliente = cveCliente_i;
				
			END IF;
		
	END CASE;

END$$

DROP PROCEDURE IF EXISTS `guardarPerfil`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `guardarPerfil` (IN `ban` INT, IN `cvePerfil_i` INT, IN `nombrePerfil_i` VARCHAR(120), IN `descipcionPerfil_i` VARCHAR(250), IN `cveOpcion_i` INT, IN `cveUsuarioAccion_i` INT)  BEGIN
	
	-- 1. Guarda un usuario y guarda cambios
	
	CASE
		WHEN ban = 1 THEN
		
			IF cvePerfil_i < 1 THEN
			
				INSERT INTO ca_perfil  
					(nombre_perfil,
					 descripcion_perfil,
					 estatus_perfil,
					 cveusuarioalta_perfil,
					 fechaalta_perfil
					) VALUES (nombrePerfil_i,
										descipcionPerfil_i,
										1,
										cveUsuarioAccion_i,
										NOW()
									 );
									 
				SELECT MAX(cve_perfil) AS cve FROM ca_perfil;
				
			ELSE
			
				UPDATE ca_perfil 
					SET nombre_perfil = nombrePerfil_i,
							descripcion_perfil = descipcionPerfil_i,
							cveusuariomod_perfil = cveUsuarioAccion_i,
							fechamod_perfil = NOW()
					WHERE cve_perfil = cvePerfil_i;
					
					SELECT cve_perfil FROM ca_perfil WHERE cve_perfil = cvePerfil_i;
				
			END IF;
			
		WHEN ban = 2 THEN
		
			INSERT INTO de_perfilopcion (cveperfil_perfilopcion,cveopcion_perfilopcion) VALUES (cvePerfil_i,cveOpcion_i);
		
	END CASE;

END$$

DROP PROCEDURE IF EXISTS `guardarProducto`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `guardarProducto` (IN `ban` INT, IN `cve_producto_i` INT, IN `nombreProducto_i` VARCHAR(100), IN `preciomayoreo_producto_i` VARCHAR(150), IN `preciomenudeo_producto_i` VARCHAR(120), IN `cveUsuarioAccion_i` INT)  BEGIN
	
	-- 1. Guarda una producto y guarda cambios
	
	CASE
		WHEN ban = 1 THEN
		
			IF cve_producto_i < 1 THEN
			
				INSERT INTO ca_productos  
					(
					nombre_producto,
					preciomayoreo_producto,
					preciomenudeo_producto,
				    cveusuarioadd_producto,
					fechaadd_productos
					) VALUES (
					                    nombreProducto_i,
										preciomayoreo_producto_i,
										preciomenudeo_producto_i,
										cveUsuarioAccion_i,
										NOW()
									 );
				
			ELSE
			
				UPDATE ca_productos 
				SET 
				        nombre_producto = nombreProducto_i,
						preciomayoreo_producto = preciomayoreo_producto_i,
						preciomenudeo_producto = preciomenudeo_producto_i,
						cveusuariomod_producto = cveUsuarioAccion_i,
						fechamod_producto = NOW()
				WHERE cve_producto = cve_producto_i;
				
			END IF;
		
	END CASE;

END$$

DROP PROCEDURE IF EXISTS `guardarPuesto`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `guardarPuesto` (IN `ban` INT, IN `cvePuesto_i` INT, IN `nombrePuesto_i` VARCHAR(100), IN `cveUsuarioAccion_i` INT)  BEGIN
	
	-- 1. Guarda un Puesto y guarda cambios
	
	CASE
		WHEN ban = 1 THEN
		
			IF cvePuesto_i < 1 THEN
			
				INSERT INTO ca_puestos  
					(
					nombre_puesto,
					estatus_puesto,
					cveusuarioalta_puesto,
					fechaalta_puesto
					) VALUES (nombrePuesto_i,
										1,
										cveUsuarioAccion_i,
										NOW()
									 );
				
			ELSE
			
				UPDATE ca_puestos 
				SET nombre_puesto = nombrePuesto_i,
						cveusuariomod_puesto = cveUsuarioAccion_i,
						fechamod_puesto = NOW()
				WHERE cve_puesto = cvePuesto_i;
				
			END IF;
		
	END CASE;

END$$

DROP PROCEDURE IF EXISTS `guardarSabor`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `guardarSabor` (IN `ban` INT, IN `cve_sabor_i` INT, IN `nombre_sabor_i` VARCHAR(100), IN `cveproducto_sabor_i` VARCHAR(150), IN `clave_sabor_i` VARCHAR(120), IN `cveUsuarioAccion_i` INT)  BEGIN
	
	-- 1. Guarda una sabor y guarda cambios
	
	CASE
		WHEN ban = 1 THEN
		
			IF cve_sabor_i < 1 THEN
			
				INSERT INTO ca_sabores  
					(
					nombre_sabor,
					cveproducto_sabor,
					clave_sabor,
					cveusuarioadd_sabor,
					fechaadd_sabor
					) VALUES (
										nombre_sabor_i,
										cveproducto_sabor_i,
										clave_sabor_i,
										cveUsuarioAccion_i,
										NOW()
									 );
				
			ELSE
			
				UPDATE ca_sabores 
				SET 	nombre_sabor = nombre_sabor_i,
						cveproducto_sabor = cveproducto_sabor_i,
						clave_sabor = clave_sabor_i,
						cveusuariomod_sabor = cveUsuarioAccion_i,
						fechamod_sabor = NOW()
				WHERE cve_sabor = cve_sabor_i;
				
			END IF;
		
	END CASE;

END$$

DROP PROCEDURE IF EXISTS `guardarSucursal`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `guardarSucursal` (IN `ban` INT, IN `cve_sucursal_i` INT, IN `nombre_sucursal_i` VARCHAR(100), IN `calle_sucursal_i` VARCHAR(150), IN `colonia_sucursal_i` VARCHAR(120), IN `telefono_sucursal_i` VARCHAR(75), IN `representante_sucursal_i` VARCHAR(120), IN `tipo_sucursal_i` INT, IN `cveUsuarioAccion_i` INT)  BEGIN
	
	-- 1. Guarda una sucursal y guarda cambios
	
	CASE
		WHEN ban = 1 THEN
		
			IF cve_sucursal_i < 1 THEN
			
				INSERT INTO ca_sucursales  
					(
					tipo_sucursal,
					nombre_sucursal,
					calle_sucursal,
					colonia_sucursal,
					telefono_sucursal,
					representante_sucursal,
					estatus_sucursal,
					cveusuarioalta_sucursal,
					fechaalta_sucursal
					) VALUES (tipo_sucursal_i,
										nombre_sucursal_i,
										calle_sucursal_i,
										colonia_sucursal_i,
										telefono_sucursal_i,
										representante_sucursal_i,
										1,
										cveUsuarioAccion_i,
										NOW()
									 );
				
			ELSE
			
				UPDATE ca_sucursales 
				SET tipo_sucursal = tipo_sucursal_i,
						nombre_sucursal = nombre_sucursal_i,
						calle_sucursal = calle_sucursal_i,
						colonia_sucursal = colonia_sucursal_i,
						telefono_sucursal = telefono_sucursal_i,
						representante_sucursal = representante_sucursal_i,
						cveusuariomod_sucursal = cveUsuarioAccion_i,
						fechamod_sucursal = NOW()
				WHERE cve_sucursal = cve_sucursal_i;
				
			END IF;
		
	END CASE;

END$$

DROP PROCEDURE IF EXISTS `guardarUsuario`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `guardarUsuario` (IN `ban` INT, IN `cveUsuario_i` INT, IN `nombreUsuario_i` VARCHAR(75), IN `apellidopUsuario_i` VARCHAR(75), IN `apellidomUsuario_i` VARCHAR(75), IN `loginUsuario_i` VARCHAR(35), IN `passwordUsuario_i` VARCHAR(75), IN `perfilUsuario_i` INT, IN `sucursal_i` INT, IN `puesto_i` INT, IN `cveUsuarioAccion_i` INT)  BEGIN

	-- 1. Guarda un usuario y guarda cambios
	
	CASE
		WHEN ban = 1 THEN
		
			IF cveUsuario_i < 1 THEN
			 
				INSERT INTO ca_usuario  
					(
					 cveperfil_usuario,
					 cvesucursal_usuario,
					 cvepuesto_usuario,
					 login_usuario,
					 password_usuario,
					 nombre_usuario,
					 apellidop_usuario,
					 apellidom_usuario,
					 estatus_usuario,
					 cveusuarioalta_usuario,
					 fechaalta_usuario
					) VALUES (perfilUsuario_i,
										sucursal_i,
										puesto_i,
										loginUsuario_i,
										passwordUsuario_i,
										nombreUsuario_i,
										apellidopUsuario_i,
										apellidomUsuario_i,
										'1',
										cveUsuarioAccion_i,
										NOW());
			
			ELSE
			
				IF passwordUsuario_i = "" THEN
				
					UPDATE ca_usuario 
					SET cveperfil_usuario = perfilUsuario_i,
							cvesucursal_usuario = sucursal_i,
							cvepuesto_usuario = puesto_i,
							login_usuario = loginUsuario_i,
							nombre_usuario = nombreUsuario_i,
							apellidop_usuario = apellidopUsuario_i,
							apellidom_usuario = apellidomUsuario_i,
							cveusuariomod_usuario = cveUsuarioAccion_i,
							fechamod_usuario = NOW()
					WHERE cve_usuario = cveUsuario_i;
					
				ELSE
				
					UPDATE ca_usuario 
					SET cveperfil_usuario = perfilUsuario_i,
							cvesucursal_usuario = sucursal_i,
							cvepuesto_usuario = puesto_i,
							login_usuario = loginUsuario_i,
							password_usuario = passwordUsuario_i,
							nombre_usuario = nombreUsuario_i,
							apellidop_usuario = apellidopUsuario_i,
							apellidom_usuario = apellidomUsuario_i,
							cveusuariomod_usuario = cveUsuarioAccion_i,
							fechamod_usuario = NOW()
					WHERE cve_usuario = cveUsuario_i;
				
				END IF;
			
			
			END IF;
			
	END CASE;

END$$

DROP PROCEDURE IF EXISTS `loginUsuario`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `loginUsuario` (IN `usuario` VARCHAR(75), IN `pass` VARCHAR(75))  BEGIN
	SELECT 
		cve_usuario,
		CONCAT(nombre_usuario, ' ', apellidop_usuario, ' ', apellidom_usuario) AS nombreCompleto,
		cveperfil_usuario,
		login_usuario, 
		(SELECT COUNT(*) FROM ca_usuario WHERE login_usuario = usuario AND password_usuario = pass AND estatus_usuario = 1) AS total_rows 
	FROM ca_usuario
	WHERE login_usuario = usuario 
	AND password_usuario = pass 
	AND estatus_usuario = 1;

END$$

DROP PROCEDURE IF EXISTS `obtenClientes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `obtenClientes` (IN `ban` INT, IN `cveCliente` INT)  BEGIN
	
	-- 1. Obtenemos todos los Clientes activos
	-- 2. Obtenemos un cliente en especifico
	
	CASE
		WHEN ban = 1 THEN
		
				SELECT 
					*
				FROM ca_clientes;
				
		WHEN ban = 2 THEN
		
				SELECT 
					*
				FROM ca_clientes 
				WHERE cve_cliente = cveCliente;
				
		END CASE;

END$$

DROP PROCEDURE IF EXISTS `obtenPerfiles`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `obtenPerfiles` (IN `ban` INT, IN `cv_perfil_i` INT)  BEGIN
	
	-- 1. Obtenemos todos los PERFILES
	-- 2. Obtenemos un perfil en especifico
	
	CASE
		WHEN ban = 1 THEN
		
				SELECT cve_perfil,nombre_perfil,descripcion_perfil,estatus_perfil FROM ca_perfil WHERE 1;
				
		WHEN ban = 2 THEN
		
				SELECT cve_perfil,nombre_perfil,descripcion_perfil,estatus_perfil FROM ca_perfil WHERE cve_perfil = cv_perfil_i;
				
	END CASE;

END$$

DROP PROCEDURE IF EXISTS `obtenProducto`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `obtenProducto` (IN `ban` INT, IN `cve_producto_i` INT)  BEGIN
	
	-- 1. Obtenemos todos losPRODUCTOS
	-- 2. Obtenemos un PRODUCTO en especifico
	
	CASE
		WHEN ban = 1 THEN
		
				SELECT 
					*
				FROM ca_productos WHERE 1;
				
		WHEN ban = 2 THEN
		
				SELECT * FROM ca_productos WHERE cve_producto = cve_producto_i;
			
		WHEN ban = 3 THEN
		
				SELECT 
					*
				FROM ca_productos WHERE estatus_producto = 1;
				
	END CASE;

END$$

DROP PROCEDURE IF EXISTS `obtenPuestos`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `obtenPuestos` (IN `ban` INT, IN `cve_puesto_i` INT)  BEGIN
	
	-- 1. Obtenemos todos los PUESTOS
	-- 2. Obtenemos un PUESTO en especifico
	
	CASE
		WHEN ban = 1 THEN
		
				SELECT 
					cve_puesto,
					nombre_puesto,
					estatus_puesto
				FROM ca_puestos WHERE 1;
				
		WHEN ban = 2 THEN
		
				SELECT cve_puesto,nombre_puesto,estatus_puesto FROM ca_puestos WHERE cve_puesto = cve_puesto_i;
				
	END CASE;

END$$

DROP PROCEDURE IF EXISTS `obtenSabores`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `obtenSabores` (IN `ban` INT, IN `cve_sabor_i` INT)  BEGIN
	
	-- 1. Obtenemos todos los SABORES
	-- 2. Obtenemos un SABOR en especifico
	
	CASE
		WHEN ban = 1 THEN
		
				SELECT 
					cs.*, cp.cve_producto, cp.nombre_producto 
				FROM ca_sabores cs
				inner join ca_productos cp on cp. cve_producto =   cs.cveproducto_sabor
				WHERE 1;
				
		WHEN ban = 2 THEN
		
				SELECT 
					cs.*, cp.cve_producto, cp.nombre_producto 
				FROM ca_sabores cs
				inner join ca_productos cp on cp. cve_producto =   cs.cveproducto_sabor
				where cve_sabor = cve_sabor_i;
	
		WHEN ban = 3 THEN
		
				SELECT 
					cs.*, cp.cve_producto, cp.nombre_producto 
				FROM ca_sabores cs
				inner join ca_productos cp on cp. cve_producto =   cs.cveproducto_sabor
				WHERE cs.estatus_sabor = 1 and cveproducto_sabor = cve_sabor_i  ;
				
				
	END CASE;

END$$

DROP PROCEDURE IF EXISTS `obtenSucursales`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `obtenSucursales` (IN `ban` INT, IN `cve_sucursal_i` INT)  BEGIN
	
	-- 1. Obtenemos todos las SUCURSALE
	-- 2. Obtenemos una SUCURSAL en especifico
	
	CASE
		WHEN ban = 1 THEN
		
				SELECT 
					cve_sucursal,
					nombre_sucursal,
					CONCAT(calle_sucursal," ",colonia_sucursal) AS direccion_sucursal,
					telefono_sucursal,
					representante_sucursal,
					IF(tipo_sucursal = 1,"SUCURSAL","ALMACÃ‰N") AS tipo_sucursal,
					estatus_sucursal 
				FROM ca_sucursales WHERE 1;
				
		WHEN ban = 2 THEN
		
				SELECT cve_sucursal,nombre_sucursal,calle_sucursal,colonia_sucursal,telefono_sucursal,representante_sucursal,tipo_sucursal FROM ca_sucursales WHERE cve_sucursal = cve_sucursal_i;
				
	END CASE;

END$$

DROP PROCEDURE IF EXISTS `obtenUsuarios`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `obtenUsuarios` (IN `ban` INT, IN `cveUsuario` INT)  BEGIN
	
	-- 1. Obtenemos todos los Usuarios activos
	-- 2. Obtenemos un usuario en especifico
	
	CASE
		WHEN ban = 1 THEN
		
				SELECT 
					a1.cve_usuario,
					CONCAT(a1.nombre_usuario,' ',a1.apellidop_usuario,' ',a1.apellidom_usuario) AS nombreCompleto,
					a1.login_usuario,
					b1.nombre_perfil,
					c1.nombre_sucursal,
					d1.nombre_puesto,
					a1.estatus_usuario
				FROM ca_usuario AS a1 
				LEFT JOIN ca_perfil AS b1 ON b1.cve_perfil = a1.cveperfil_usuario
				LEFT JOIN ca_sucursales AS c1 ON c1.cve_sucursal = a1.cvesucursal_usuario
				LEFT JOIN ca_puestos AS d1 ON d1.cve_puesto = a1.cvepuesto_usuario
				WHERE 1 ORDER BY a1.nombre_usuario;
				
		WHEN ban = 2 THEN
		
				SELECT 
					a1.cve_usuario,
					a1.nombre_usuario,
					a1.apellidop_usuario,
					a1.apellidom_usuario,
					a1.login_usuario,
					a1.cveperfil_usuario,
					a1.cvesucursal_usuario,
					a1.cvepuesto_usuario,
					b1.nombre_perfil,
					c1.nombre_sucursal,
					d1.nombre_puesto
				FROM ca_usuario AS a1 
				LEFT JOIN ca_perfil AS b1 ON b1.cve_perfil = a1.cveperfil_usuario
				LEFT JOIN ca_sucursales AS c1 ON c1.cve_sucursal = a1.cvesucursal_usuario
				LEFT JOIN ca_puestos AS d1 ON d1.cve_puesto = a1.cvepuesto_usuario
				WHERE a1.cve_usuario = cveUsuario;
				
		END CASE;

END$$

DROP PROCEDURE IF EXISTS `obten_opcionesperfil`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `obten_opcionesperfil` (IN `ban` INT, IN `cvePerfil_i` INT, IN `opcion_i` INT)  BEGIN
	
	-- 1. Obtenemos todos las opciones R ordenados dependiendo el perfil del usuario
	-- 2. Obtenemos todos las opciones M ordenados dependiendo la opcion R y el perfil del usuario
	-- 3. Obtenemos todos las opciones R ordenados
	-- 4. Obtenemos todos las opciones M ordenados dependiendo la opcion R
	-- 5. Obtenemos todos las opciones M ordenados dependiendo la opcion R y el perfil del usuario comparando con los ya guardados
	-- 6. Obtenemos todas las opciones dependiendo el perfil
	
	CASE
		WHEN ban = 1 THEN
		
				SELECT 
					cve_opcion,
					nombre_opcion,
					icono,
					render_opcion,
					orden_opcion
				FROM ca_opcion
				WHERE cve_opcion 
				IN (SELECT b.cveopcion_opcion
				FROM de_perfilopcion AS a
				INNER JOIN ca_opcion AS b ON b.cve_opcion = a.cveopcion_perfilopcion
				WHERE a.cveperfil_perfilopcion = cvePerfil_i GROUP BY b.cveopcion_opcion)
				
				ORDER BY orden_opcion ASC;
				
		WHEN ban = 2 THEN
		
				SELECT 
					cao.cve_opcion,
					cao.nombre_opcion,
					cao.metodo_opcion,
					cao.render_opcion,
					cao.orden_opcion
				FROM ca_opcion AS cao
				INNER JOIN de_perfilopcion AS dep ON dep.cveopcion_perfilopcion = cao.cve_opcion
				WHERE dep.cveperfil_perfilopcion = cvePerfil_i AND cao.cveopcion_opcion = opcion_i;
				
		WHEN ban = 3 THEN
		
				SELECT 
					cve_opcion,
					nombre_opcion,
					icono,
					render_opcion,
					orden_opcion
				FROM ca_opcion
				WHERE render_opcion = 'R'
				
				ORDER BY orden_opcion ASC;
				
		WHEN ban = 4 THEN
		
				SELECT 
					cao.cve_opcion,
					cao.nombre_opcion,
					cao.metodo_opcion,
					cao.render_opcion,
					cao.orden_opcion
				FROM ca_opcion AS cao
				-- INNER JOIN de_perfilopcion AS dep ON dep.cveopcion_perfilopcion = cao.cve_opcion
				WHERE cao.cveopcion_opcion = opcion_i;
				
		WHEN ban = 5 THEN
		
				SELECT 
					cao.cve_opcion,
					cao.nombre_opcion,
					cao.metodo_opcion,
					cao.render_opcion,
					cao.orden_opcion,
					IF((SELECT COUNT(*) AS existe FROM de_perfilopcion WHERE cveperfil_perfilopcion = cvePerfil_i AND cveopcion_perfilopcion = cao.cve_opcion) > 0,1,0) AS chk
				FROM ca_opcion AS cao
				-- INNER JOIN de_perfilopcion AS dep ON dep.cveopcion_perfilopcion = cao.cve_opcion
				WHERE cao.cveopcion_opcion = opcion_i;
				
		WHEN ban = 6 THEN
		
				SELECT * FROM de_perfilopcion WHERE cveperfil_perfilopcion = cvePerfil_i;
				
		END CASE;

END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ca_clientes`
--

DROP TABLE IF EXISTS `ca_clientes`;
CREATE TABLE `ca_clientes` (
  `cve_cliente` int(11) NOT NULL,
  `nombre_cliente` varchar(100) NOT NULL,
  `celular_cliente` varchar(10) NOT NULL,
  `direccion_cliente` varchar(250) NOT NULL,
  `comentario_cliente` varchar(500) NOT NULL,
  `cveusuarioadd_cliente` int(11) NOT NULL,
  `fechaadd_cliente` datetime NOT NULL DEFAULT current_timestamp(),
  `cveusuariomod_cliente` int(11) DEFAULT NULL,
  `fechamod_cliente` datetime DEFAULT NULL,
  `estatus_cliente` varchar(100) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `ca_clientes`
--

INSERT INTO `ca_clientes` (`cve_cliente`, `nombre_cliente`, `celular_cliente`, `direccion_cliente`, `comentario_cliente`, `cveusuarioadd_cliente`, `fechaadd_cliente`, `cveusuariomod_cliente`, `fechamod_cliente`, `estatus_cliente`) VALUES
(1, 'Nombre', '6688524855', 'dirección', 'comentario', 1, '2021-07-05 22:40:27', 1, '2021-07-14 20:58:33', '1'),
(2, 'CLIENTE', '334343', 'DIRECCIÓN', 'COMENTARIOF', 1, '2021-07-14 21:41:02', 1, '2021-07-14 22:05:27', '1'),
(3, 'JORGE', '8432983', 'CAMPRESTRE', 'JORGEVALENZUELA', 1, '2021-07-14 22:02:28', NULL, NULL, '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ca_opcion`
--

DROP TABLE IF EXISTS `ca_opcion`;
CREATE TABLE `ca_opcion` (
  `cve_opcion` int(11) NOT NULL,
  `nombre_opcion` varchar(120) DEFAULT NULL,
  `descripcion_opcion` varchar(255) DEFAULT NULL,
  `cveopcion_opcion` int(5) DEFAULT 0,
  `archivo_opcion` varchar(120) DEFAULT NULL,
  `metodo_opcion` varchar(120) DEFAULT NULL,
  `icono` varchar(120) DEFAULT NULL COMMENT 'ICONOS DE LA MISMA LIBRERIA',
  `orden_opcion` int(5) DEFAULT 0,
  `render_opcion` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `ca_opcion`
--

INSERT INTO `ca_opcion` (`cve_opcion`, `nombre_opcion`, `descripcion_opcion`, `cveopcion_opcion`, `archivo_opcion`, `metodo_opcion`, `icono`, `orden_opcion`, `render_opcion`) VALUES
(1, 'ADMINISTRACIÓN', 'SECCIÓN PARA LA ADMINISTRACIÓN DE CATÁLOGOS, PERFILES Y USUARIOS', 0, NULL, NULL, 'fa fa-cogs', 10, 'R'),
(2, 'USUARIO', 'MÓDULO PARA AGREGAR USUARIOS Y ASIGNAR PERFILES', 1, 'Usuario.php', 'usuario', NULL, 1, 'M'),
(3, 'PERFIL', 'MÓDULO PARA GENERAR PERFILES Y DAR ACCESOS', 1, 'Perfil.php', 'perfil', NULL, 2, 'M'),
(4, 'SUCURSALES', 'MÓDULO PARA AGREGAR SUCURSALES', 1, 'Sucursal.php', 'sucursal', NULL, 3, 'M'),
(5, 'PUESTOS', 'MÓDULO', 1, 'Puesto.php', 'puesto', NULL, 4, 'M'),
(6, 'MODULOS', 'SECCIÓN PARA LA ADMINISTRACIÓN DE PRODUCTOS, SABORES Y CLIENTES', 0, NULL, NULL, 'fa fa-cogs', 9, 'R'),
(7, 'PRODUCTOS', 'MÓDULO PARA AGREGAR PRODUCTOS', 6, 'Producto.php', 'producto', NULL, 1, 'M'),
(8, 'SABORES', 'MÓDULO PARA AGREGAR SABORES', 6, 'Sabor.php', 'sabor', NULL, 2, 'M'),
(9, 'CLIENTES', 'MÓDULO PARA AGREGAR CLIENTES', 6, 'Cliente.php', 'cliente', NULL, 3, 'M');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ca_perfil`
--

DROP TABLE IF EXISTS `ca_perfil`;
CREATE TABLE `ca_perfil` (
  `cve_perfil` int(11) NOT NULL,
  `nombre_perfil` varchar(120) DEFAULT NULL,
  `descripcion_perfil` varchar(255) DEFAULT NULL,
  `estatus_perfil` int(1) DEFAULT 0,
  `cveusuarioalta_perfil` int(5) DEFAULT 0,
  `fechaalta_perfil` datetime DEFAULT NULL,
  `cveusuariomod_perfil` int(5) DEFAULT NULL,
  `fechamod_perfil` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `ca_perfil`
--

INSERT INTO `ca_perfil` (`cve_perfil`, `nombre_perfil`, `descripcion_perfil`, `estatus_perfil`, `cveusuarioalta_perfil`, `fechaalta_perfil`, `cveusuariomod_perfil`, `fechamod_perfil`) VALUES
(1, 'SYSADMIN', 'ACCESO A TODOS LOS MÓDULOS DEL SISTEMA', 1, 1, '2019-07-21 13:24:19', 1, '2021-07-08 21:58:38'),
(2, 'CAJERO', 'ACCESO SOLO A MAESTROS', 1, 1, '2019-08-02 20:46:18', 1, '2021-06-22 03:40:05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ca_productos`
--

DROP TABLE IF EXISTS `ca_productos`;
CREATE TABLE `ca_productos` (
  `cve_producto` int(11) NOT NULL,
  `nombre_producto` varchar(100) NOT NULL,
  `preciomayoreo_producto` double NOT NULL,
  `preciomenudeo_producto` double NOT NULL,
  `estatus_producto` int(11) NOT NULL DEFAULT 1,
  `cveusuarioadd_producto` int(11) NOT NULL,
  `fechaadd_productos` datetime NOT NULL DEFAULT current_timestamp(),
  `cveusuariomod_producto` int(11) DEFAULT NULL,
  `fechamod_producto` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `ca_productos`
--

INSERT INTO `ca_productos` (`cve_producto`, `nombre_producto`, `preciomayoreo_producto`, `preciomenudeo_producto`, `estatus_producto`, `cveusuarioadd_producto`, `fechaadd_productos`, `cveusuariomod_producto`, `fechamod_producto`) VALUES
(1, 'PALETA DE HIELO', 5, 10, 1, 1, '2021-07-03 17:07:11', 1, '2021-07-17 15:18:57'),
(2, 'BOLIS', 2, 6, 1, 1, '2021-07-07 22:04:16', NULL, NULL),
(3, 'CHEMO', 7, 13, 1, 1, '2021-07-12 18:57:59', 1, '2021-07-20 22:09:24'),
(4, 'ESQUIMAL', 12.5, 6.5, 1, 1, '2021-07-12 19:02:38', 1, '2021-07-17 15:19:01'),
(5, 'MANGONEADA', 12.5, 6.5, 1, 1, '2021-07-12 19:02:56', NULL, NULL),
(6, 'PALETA DE CREMA', 8, 5, 1, 1, '2021-07-12 19:45:23', 1, '2021-07-17 15:19:03');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ca_puestos`
--

DROP TABLE IF EXISTS `ca_puestos`;
CREATE TABLE `ca_puestos` (
  `cve_puesto` int(255) NOT NULL,
  `nombre_puesto` varchar(120) DEFAULT NULL,
  `estatus_puesto` int(1) DEFAULT NULL,
  `cveusuarioalta_puesto` int(5) DEFAULT NULL,
  `fechaalta_puesto` datetime DEFAULT NULL,
  `cveusuariomod_puesto` int(5) DEFAULT NULL,
  `fechamod_puesto` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `ca_puestos`
--

INSERT INTO `ca_puestos` (`cve_puesto`, `nombre_puesto`, `estatus_puesto`, `cveusuarioalta_puesto`, `fechaalta_puesto`, `cveusuariomod_puesto`, `fechamod_puesto`) VALUES
(1, 'ADMINISTRADOR', 1, 1, '2019-08-04 05:47:28', NULL, NULL),
(2, 'CAJERO', 1, 1, '2019-08-04 06:27:42', 1, '2019-08-04 06:40:42');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ca_sabores`
--

DROP TABLE IF EXISTS `ca_sabores`;
CREATE TABLE `ca_sabores` (
  `cve_sabor` int(11) NOT NULL,
  `nombre_sabor` varchar(100) NOT NULL,
  `cveproducto_sabor` int(11) NOT NULL,
  `clave_sabor` varchar(10) NOT NULL,
  `estatus_sabor` int(11) NOT NULL DEFAULT 1 COMMENT '1 = Activo 2 = Inactivo',
  `cveusuarioadd_sabor` int(11) NOT NULL,
  `fechaadd_sabor` datetime NOT NULL DEFAULT current_timestamp(),
  `cveusuariomod_sabor` int(11) DEFAULT NULL,
  `fechamod_sabor` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `ca_sabores`
--

INSERT INTO `ca_sabores` (`cve_sabor`, `nombre_sabor`, `cveproducto_sabor`, `clave_sabor`, `estatus_sabor`, `cveusuarioadd_sabor`, `fechaadd_sabor`, `cveusuariomod_sabor`, `fechamod_sabor`) VALUES
(1, 'UVA', 2, 'U', 1, 1, '2021-07-15 22:10:27', 1, '2021-07-15 23:52:20'),
(2, 'MELON', 3, 'MEL', 1, 1, '2021-07-20 22:09:00', NULL, NULL),
(3, 'FRESA', 6, 'FR', 1, 1, '2021-07-20 22:10:38', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ca_sucursales`
--

DROP TABLE IF EXISTS `ca_sucursales`;
CREATE TABLE `ca_sucursales` (
  `cve_sucursal` int(11) NOT NULL,
  `tipo_sucursal` int(2) DEFAULT NULL COMMENT '1-Sucursal, 2-AlmacÃ©n',
  `nombre_sucursal` varchar(150) DEFAULT NULL,
  `calle_sucursal` varchar(255) DEFAULT NULL,
  `colonia_sucursal` varchar(75) DEFAULT NULL,
  `telefono_sucursal` varchar(75) DEFAULT NULL,
  `representante_sucursal` varchar(120) DEFAULT NULL,
  `estatus_sucursal` int(1) DEFAULT NULL,
  `cveusuarioalta_sucursal` int(5) DEFAULT NULL,
  `fechaalta_sucursal` datetime DEFAULT NULL,
  `cveusuariomod_sucursal` int(5) DEFAULT NULL,
  `fechamod_sucursal` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `ca_sucursales`
--

INSERT INTO `ca_sucursales` (`cve_sucursal`, `tipo_sucursal`, `nombre_sucursal`, `calle_sucursal`, `colonia_sucursal`, `telefono_sucursal`, `representante_sucursal`, `estatus_sucursal`, `cveusuarioalta_sucursal`, `fechaalta_sucursal`, `cveusuariomod_sucursal`, `fechamod_sucursal`) VALUES
(1, 1, 'HELADOS ALTEÑA PRINCIPAL', 'AV. LIBRAMIENTO 2, ESQUINA CON AV. MANUEL J. CLOUTHIER #100', 'FRACC, LOS CONCHIS', '6692158585', 'DIR. MIGUEL ANGEL MEDRANO ROMERO', 1, 1, '2019-08-02 23:03:21', 1, '2021-07-15 21:21:34');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ca_usuario`
--

DROP TABLE IF EXISTS `ca_usuario`;
CREATE TABLE `ca_usuario` (
  `cve_usuario` int(11) NOT NULL,
  `cveperfil_usuario` int(5) DEFAULT 0,
  `cvesucursal_usuario` int(5) DEFAULT NULL,
  `cvepuesto_usuario` int(5) DEFAULT NULL,
  `login_usuario` varchar(120) DEFAULT NULL,
  `password_usuario` varchar(120) DEFAULT NULL,
  `nombre_usuario` varchar(75) DEFAULT NULL,
  `apellidop_usuario` varchar(75) DEFAULT NULL,
  `apellidom_usuario` varchar(75) DEFAULT NULL,
  `estatus_usuario` int(1) DEFAULT 0,
  `cveusuarioalta_usuario` int(11) DEFAULT NULL,
  `fechaalta_usuario` datetime DEFAULT NULL,
  `cveusuariomod_usuario` int(11) DEFAULT NULL,
  `fechamod_usuario` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `ca_usuario`
--

INSERT INTO `ca_usuario` (`cve_usuario`, `cveperfil_usuario`, `cvesucursal_usuario`, `cvepuesto_usuario`, `login_usuario`, `password_usuario`, `nombre_usuario`, `apellidop_usuario`, `apellidom_usuario`, `estatus_usuario`, `cveusuarioalta_usuario`, `fechaalta_usuario`, `cveusuariomod_usuario`, `fechamod_usuario`) VALUES
(1, 1, 0, 1, 'jorge', 'b3743d2588b110cc6f2ba43f1758a757', 'JORGE', 'VALENZUELA', 'SANTIAGO', 1, 1, '2019-07-27 00:45:44', 1, '2019-08-06 13:52:21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `de_perfilopcion`
--

DROP TABLE IF EXISTS `de_perfilopcion`;
CREATE TABLE `de_perfilopcion` (
  `cve_perfilopcion` int(11) NOT NULL,
  `cveperfil_perfilopcion` int(11) DEFAULT NULL,
  `cveopcion_perfilopcion` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `de_perfilopcion`
--

INSERT INTO `de_perfilopcion` (`cve_perfilopcion`, `cveperfil_perfilopcion`, `cveopcion_perfilopcion`) VALUES
(189, 2, 3),
(190, 2, 4),
(200, 1, 2),
(201, 1, 3),
(202, 1, 4),
(203, 1, 7),
(204, 1, 8),
(205, 1, 9),
(206, 1, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `de_venta`
--

DROP TABLE IF EXISTS `de_venta`;
CREATE TABLE `de_venta` (
  `cve_venta` int(11) NOT NULL,
  `cveventa_venta` int(11) NOT NULL,
  `cvesp_venta` int(11) NOT NULL COMMENT 'son las claves primarias de ca_productos y ca_sabores',
  `cantidad_venta` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ma_ventas`
--

DROP TABLE IF EXISTS `ma_ventas`;
CREATE TABLE `ma_ventas` (
  `cve_ventas` int(11) NOT NULL,
  `folio_venta` varchar(10) NOT NULL,
  `total_venta` double NOT NULL,
  `tipo_venta` int(11) NOT NULL COMMENT '1=venta 2=domicilio',
  `cvecliente_venta` int(11) NOT NULL COMMENT 'Llave foranea de ca_cliente',
  `cveusuarioadd_venta` int(11) NOT NULL,
  `fechaadd_venta` datetime NOT NULL DEFAULT current_timestamp(),
  `cveusuariomod_venta` int(11) NOT NULL,
  `fechamod_venta` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `ca_clientes`
--
ALTER TABLE `ca_clientes`
  ADD PRIMARY KEY (`cve_cliente`);

--
-- Indices de la tabla `ca_opcion`
--
ALTER TABLE `ca_opcion`
  ADD PRIMARY KEY (`cve_opcion`) USING BTREE;

--
-- Indices de la tabla `ca_perfil`
--
ALTER TABLE `ca_perfil`
  ADD PRIMARY KEY (`cve_perfil`) USING BTREE;

--
-- Indices de la tabla `ca_productos`
--
ALTER TABLE `ca_productos`
  ADD PRIMARY KEY (`cve_producto`);

--
-- Indices de la tabla `ca_puestos`
--
ALTER TABLE `ca_puestos`
  ADD PRIMARY KEY (`cve_puesto`) USING BTREE;

--
-- Indices de la tabla `ca_sabores`
--
ALTER TABLE `ca_sabores`
  ADD PRIMARY KEY (`cve_sabor`);

--
-- Indices de la tabla `ca_sucursales`
--
ALTER TABLE `ca_sucursales`
  ADD PRIMARY KEY (`cve_sucursal`) USING BTREE;

--
-- Indices de la tabla `ca_usuario`
--
ALTER TABLE `ca_usuario`
  ADD PRIMARY KEY (`cve_usuario`) USING BTREE;

--
-- Indices de la tabla `de_perfilopcion`
--
ALTER TABLE `de_perfilopcion`
  ADD PRIMARY KEY (`cve_perfilopcion`) USING BTREE;

--
-- Indices de la tabla `ma_ventas`
--
ALTER TABLE `ma_ventas`
  ADD PRIMARY KEY (`cve_ventas`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `ca_clientes`
--
ALTER TABLE `ca_clientes`
  MODIFY `cve_cliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `ca_opcion`
--
ALTER TABLE `ca_opcion`
  MODIFY `cve_opcion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `ca_perfil`
--
ALTER TABLE `ca_perfil`
  MODIFY `cve_perfil` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `ca_productos`
--
ALTER TABLE `ca_productos`
  MODIFY `cve_producto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `ca_puestos`
--
ALTER TABLE `ca_puestos`
  MODIFY `cve_puesto` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `ca_sabores`
--
ALTER TABLE `ca_sabores`
  MODIFY `cve_sabor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `ca_sucursales`
--
ALTER TABLE `ca_sucursales`
  MODIFY `cve_sucursal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `ca_usuario`
--
ALTER TABLE `ca_usuario`
  MODIFY `cve_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `de_perfilopcion`
--
ALTER TABLE `de_perfilopcion`
  MODIFY `cve_perfilopcion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=207;

--
-- AUTO_INCREMENT de la tabla `ma_ventas`
--
ALTER TABLE `ma_ventas`
  MODIFY `cve_ventas` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


