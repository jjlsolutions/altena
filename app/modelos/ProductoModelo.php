<?php

class ProductoModelo
{

	//creamos la variable donde se instanciará la clase "conectar"
    public $conexion;

    public function __construct() {

    	//inicializamos la clase para conectarnos a la bd
        $this->conexion = new ConexionBD(); //instanciamos la clase

    }



    public function consultar($datos)
    {
        $datosFiltrados = $this->filtrarDatos($datos);

        $ban  = $datosFiltrados['ban'];
        $cve_producto = (!empty($datosFiltrados['cve_producto']) || $datosFiltrados['cve_producto']!=null) ? $datosFiltrados['cve_producto'] : '0';

        $query = "CALL obtenProducto('$ban','$cve_producto')";
        //echo $query;

        $c_producto = $this->conexion->query($query);
        $r_producto = $this->conexion->consulta_array($c_producto);

        return $r_producto;
    }



    public function guardarProducto($datosProducto)
    {

        $datosFiltrados = $this->filtrarDatos($datosProducto);

        $ban                    = $datosFiltrados['ban'];
        $nombre_producto         = $datosFiltrados['nombre_producto'];
        $preciomenudeo_producto          = $datosFiltrados['preciomenudeo_producto'];
        $preciomayoreo_producto          = $datosFiltrados['preciomayoreo_producto'];
        $cveProducto            = $datosFiltrados['cve_producto'];
        $cveusuario_accion      = $datosFiltrados['cveusuario_accion'];

        $query = "CALL guardarProducto(
                                    '$ban',
                                    '$cveProducto',
                                    '$nombre_producto',
                                    '$preciomenudeo_producto',
                                    '$preciomayoreo_producto',
                                    '$cveusuario_accion'
                                    )";

        $respuesta = $this->conexion->query($query) or die ($this->conexion->error());
        
        $this->conexion->close_conexion();
        
        return $respuesta;

    }



    public function bloquearProducto($datosProducto)
    {
        $datosFiltrados = $this->filtrarDatos($datosProducto);

        $ban               = $datosFiltrados['ban'];
        $cve_producto        = $datosFiltrados['cve_producto'];
        $cveusuario_accion = $datosFiltrados['cveusuario_accion'];

        $query = "CALL eliminarProducto('$ban','$cve_producto','$cveusuario_accion')";

        $respuesta = $this->conexion->query($query);

        return $respuesta;
    }

    

    public function filtrarDatos($datosFiltrar){

        foreach ($datosFiltrar as $indice => $valor) {
            $datosFiltrarr[$indice] = $this->conexion->real_escape_string($valor);
        }

        return $datosFiltrarr;

    }
	
}

?>