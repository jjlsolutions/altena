<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo NOMBRE_SITIO; ?> | Clientes</title>
    <?php 
    include RUTA_APP . 'vistas/includes/link.php'; 
    ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <?php 
    include RUTA_APP . 'vistas/includes/header.php';

    include RUTA_APP . 'vistas/includes/left_sidebar_menu.php'; 
    ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Clientes
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <button class="btn btn-primary" id="btnMostraFormCliente">Nuevo cliente</button>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                    <table id="gridCliente" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Nombre&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                <th>Dirección&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                <th>Celular&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                <th>Comentario</th>
                                <th>Editar</th>
                                <th>Estatus</th>
                            </tr>
                        </thead>
                    </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->

                <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->

    </div>
    <!-- /.content-wrapper -->
    
    <?php 
    //include RUTA_APP . 'vistas/includes/footer.php';

    include RUTA_APP . 'vistas/includes/control_sidebar_right.php';
    ?>

</div>
<!-- ./wrapper -->

<!-- modales -->
<div class="modal fade" id="modal_formCliente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" >
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel">Cliente</h5>
            </div>
            <div class="modal-body" id="muestra_formCliente"> 

            </div>
        </div>
    </div>
</div>

<?php 
include RUTA_APP . 'vistas/includes/script.php';
?>

<script type="text/javascript">

    $(document).ready(function () {

        tableClientes = $('#gridCliente').DataTable( {    
            "responsive": true,
            "searching" : true,
            "paging"    : true,
            "ordering"  : false,
            "info"      : true,
            "autoWidth": false,
            "columnDefs": [
                {"width": "10%","className": "text-center","targets": 4},
                {"width": "10%","className": "text-center","targets": 5},
            ],

            "bJQueryUI":true,"oLanguage": {
                "sEmptyTable":     "No hay datos registrados en la Base de Datos.",
                "sInfo":           "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando desde 0 hasta 0 de 0 registros",
                "sInfoFiltered":   "(filtrado de _MAX_ registros en total)",
                "sInfoPostFix":    "",
                "sInfoThousands":  ",",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sLoadingRecords": "Cargando...",
                "sProcessing":     "Procesando...",
                "sSearch":         "Buscar:",
                "sZeroRecords":    "No se encontraron resultados",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": activar para Ordenar Ascendentemente",
                    "sSortDescending": ": activar para Ordendar Descendentemente"
                }
            }
        });

        //Mandamos llamar la función para mostrar tabla al cargar la página
        cargarTablaCliente();

    });

    function cargarTablaCliente()
    {
        $.ajax({
            url      : 'cliente/consultar',
            type     : "POST",
            data    : { ban: 1 },
            beforeSend: function() {
                // setting a timeout

            },
            success  : function(datos) {

                var myJson = JSON.parse(datos);

                tableClientes.clear().draw();

                if(myJson.arrayDatos.length > 0)
                {

                    var title;
                    var icon;
                    var color_icon;
                    var accion;

                    $(myJson.arrayDatos).each( function(key, val)
                    {

                        if (parseInt(val.estatus_cliente) == 1)
                        {
                            title = 'Cliente activo';
                            icon = 'fa fa-circle';
                            color_icon = "color: #4ad129;"
                            accion = "bloquearCliente('" + val.cve_cliente + "','0')";
                        }
                        else
                        {
                            title = 'Cliente bloqueado';
                            icon = 'fa fa-circle';
                            color_icon = "color: #f00;"
                            accion = "bloquearCliente('" + val.cve_cliente + "','1')";
                        }

                        var btn_editar = "<i class='fa fa-edit' style='font-size:18px; cursor: pointer;' title='Editar Cliente' onclick=\"mostrarCliente('" + val.cve_cliente + "')\"></i>";
                        var btn_status = "<i class='" + icon + "' style='font-size:14px; " + color_icon + " cursor: pointer;' title='" + title + "' onclick=\"" + accion + "\"></i>";

                        tableClientes.row.add([
                            val.nombre_cliente,
                            val.direccion_cliente,
                            val.celular_cliente,
                            val.comentario_cliente,
                            btn_editar,
                            btn_status,
                        ]).draw();
                    })

                }
                else
                {
                    tableClientes = $('#gridCliente').DataTable();
                    
                }

            }
        });
    }

    $('#btnMostraFormCliente').click(function (e) {

        $('#modal_formCliente').modal({
            keyboard: false
        });

        $("#muestra_formCliente").html('Cargando...');

        $.ajax({
            url: 'cliente/formCliente',
            success: function(datos){

                $("#muestra_formCliente").html(datos);

            }
        });
        return false;
    });

    function mostrarCliente(cve_cliente)
    {
        $('#msgAlert').css("display", "none");

        $.ajax({
            url      : 'cliente/consultar',
            type     : "POST",
            data     : { 

                    ban: 2, 
                    cve_cliente: cve_cliente 

            },
            beforeSend: function() {
                // setting a timeout

            },
            success  : function(datos) {

                var myJson = JSON.parse(datos);

                console.log(myJson);

                $('#modal_formCliente').modal({
                    keyboard: false
                });

                $("#muestra_formCliente").html('Cargando...');

                $.ajax({
                    url: 'cliente/formCliente',
                    success: function(datos){

                        
                        $("#muestra_formCliente").html(datos);

                        
                        $('#nombre_cliente').val(myJson.arrayDatos[0].nombre_cliente);
                        $('#direccion_cliente').val(myJson.arrayDatos[0].direccion_cliente);
                        $('#comentario_cliente').val(myJson.arrayDatos[0].comentario_cliente);
                        $('#celular_cliente').val(myJson.arrayDatos[0].celular_cliente);
                        $('#cve_cliente').val(myJson.arrayDatos[0].cve_cliente);

                        $("#btnGuardar").html('Actualizar Cliente');
                        
                    }
                });

            }
        });
    }

    function bloquearCliente(cve_cliente,bloqueo)
    {

        if (bloqueo == 0)
        {
            var msg = "Esta seguro de bloquear esta cliente?";
            var ban = 2;
        }else{
            var msg = "Esta seguro de desbloquear esta cliente?";
            var ban = 3;
        }

        bootbox.confirm({
            message: msg,
            buttons: {
                confirm: {
                    label: 'Si'
                },
                cancel: {
                    label: 'No'
                }
            },
            callback: function (result) {
                if (result == true){

                    $.ajax({
                        url      : 'cliente/bloquearCliente',
                        type     : "POST",
                        data     : { 

                                ban: ban, 
                                cve_cliente: cve_cliente 

                        },
                        beforeSend: function() {
                            // setting a timeout

                        },
                        success  : function(datos) {

                            var myJson = JSON.parse(datos);
                    
                            if(myJson.status == "success")
                            {

                                //var table = $('#gridCliente').DataTable();
                                        
                                //table.clear();
                                //table.destroy();

                                //Reinicializamos tabla
                                cargarTablaCliente();

                                msgAlert(myJson.msg ,"info");
                                setTimeout(function() { $("#msgAlert").fadeOut(1500); },3000);

                            }

                        }
                    });

                }else{
                    //No se hace nada...
                }
            }
        });

    }


    function msgAlert(msg,tipo)
    {
        $('#msgAlert').css("display", "block");
        $("#msgAlert").html("<div class='alert alert-" + tipo + "' role='alert'>" + msg + " <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button> </div>");
    }

</script>

</body>
</html>
