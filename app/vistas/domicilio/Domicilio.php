<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>AdminLTE 3 | UI Sliders</title>
    <?php 
        include RUTA_APP . 'vistas/includes/link.php';
    ?>
    <style>
        .panel-body .btn:not(.btn-block) { width:40%;margin-bottom:16px;margin-left:16px;}
    </style>
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  
    <?php 
    include RUTA_APP . 'vistas/includes/header.php';

    include RUTA_APP . 'vistas/includes/left_sidebar_menu.php'; 
    ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Domicilios</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <form id="formUsuario" action="domicilio/guardarProducto" method="post">
            <div class="card-body panel-body" id="botones">
                
            </div>
        
            <!--<div class="box-footer">
                <button type="button" class="btn btn-primary" onclick="guardar()">Guardar</button>
                <button type="button" class="btn btn-primary" id="btnLimpiar">Limpiar</button>
                <button class="btn btn-primary" id="btnCancelar" onclick="cancelar();" style="visibility: hidden;">Cancelar</button>
            </div>-->
        </form>
        
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <button type="button" class="btn btn-block btn-success btn-lg" onclick="guardar()">COBRAR</button>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<div class="modal fade" id="modal_formCantidad">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title" id="nombre_sabor"></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <input type="hidden" id="cve_sabor" name="cve_sabor">
                <div class="row">
                    <div class="form-group col-md-12">
                        <label>Cantidad*</label>
                        <input type="number" min="0" class="form-control" id="cantidad_sabor" name="cantidad_sabor" onkeyup='javascript:this.value=this.value.toUpperCase();'>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-primary" onclick="pasarSpan()" style="width:40%;">OK</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
<!-- ./wrapper -->
    <?php 
    include RUTA_APP . 'vistas/includes/script.php';
    ?>
<script type="text/javascript">

$(document).ready(function () {
    cargarBotonesProducto();
});
function guardar(){
    $("#formUsuario").find('a').each(function() {
         var elemento= this;
         var cantidad_producto = $(elemento).find('span').text(); 

         if(cantidad_producto != ''){
            alert("cantidad_producto="+ cantidad_producto +" elemento.id="+ elemento.id + ", elemento.value=" + elemento.value); 
         }
         
        });
}
function cargarBotonesProducto(){
    $.ajax({
        url      : 'Producto/consultar',
        type     : "POST",
        data    : { 
            ban: 3
        },
        success  : function(datos) {

            var myJson = JSON.parse(datos);

            botones = $("#botones");
            $("#botones").empty();

            precioProductos = $("#precioProductos");
            $("#precioProductos").empty();

            if(myJson.arrayDatos.length > 0)
            {
                $(myJson.arrayDatos).each( function(key, val)
                {
                    botones.append('<a class="btn btn-app bg-info" id="producto_' + val.cve_producto + '" onClick="cantidad(' + val.cve_producto + ',\'' + val.nombre_producto + '\')"><b>' + val.nombre_producto + '</b></a>');
                    precioProductos.append('<p>'+val.nombre_producto+' -> '+val.precio_producto+'<small class="badge badge-success"><i class="far fa-clock"></i> 3 days</small></p>');
                });

            }

            

        }
    });
}

function cantidad(cve_producto, nombre_producto){
  cargarBotonesSabor(cve_producto);
   
}
function modalCantidad(cve_sabor, nombre_sabor){
    $('#cve_sabor').val(cve_sabor);
    $('#nombre_sabor').text(nombre_sabor);
    
    $('#modal_formCantidad').modal({
        keyboard: false,
    });
    $('#modal_formCantidad').on('shown.bs.modal', function () {
      $('#cantidad_sabor').focus();
    });
}
function cargarBotonesSabor(cve_producto){
    $.ajax({
        url      : 'Sabor/consultar',
        type     : "POST",
        data    : { 
            ban: 3, 
            cve_sabor: cve_producto
        },
        success  : function(datos) {

            var myJson = JSON.parse(datos);

            botones = $("#botones");
            $("#botones").empty();

            if(myJson.arrayDatos.length > 0)
            {
                $(myJson.arrayDatos).each( function(key, val)
                {
                    botones.append('<a class="btn btn-app bg-info" id="sabor_' + val.cve_sabor + '" onClick="modalCantidad(' + val.cve_sabor + ',\'' + val.nombre_sabor + '\')"><b>' + val.nombre_sabor + '</b></a>');
                });

            }

            

        }
    });
}

function pasarSpan(){
    cve_producto = $('#cve_producto').val();
    cantidad_producto = $("#cantidad_producto").val();

    $("#producto_"+cve_producto).find("span").remove();//remove span elements    
    $("#producto_"+cve_producto).append('<span class="badge bg-danger" style="font-size: 130%"><b>'+cantidad_producto+'</b></span>');
    $("#cantidad_producto").val("");
    $('#modal_formCantidad').modal('hide');
    
}

</script>
</body>
</html>
