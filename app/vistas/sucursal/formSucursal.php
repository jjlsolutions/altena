<?php
//print_r($datos);
?>
<div id="msgAlert2"></div>

<form id="formSucursal" action="sucursal/guardarSucursal" method="post">

    <div class="box-body">
        <div class="row">

            <div class="form-group col-md-6">
                <label>Nombre de la escuela*</label>
                <input type="text" class="form-control" id="nombre_sucursal" name="nombre_sucursal" onkeyup='javascript:this.value=this.value.toUpperCase();'>
            </div>

        </div>

        <div class="row">

            <div class="form-group col-md-6">
                <label>Dirección*</label>
                <input type="text" class="form-control" id="calle_sucursal" name="calle_sucursal" onkeyup='javascript:this.value=this.value.toUpperCase();'>
            </div>

            <div class="form-group col-md-6">
                <label>Colonia*</label>
                <input type="text" class="form-control" id="colonia_sucursal" name="colonia_sucursal" onkeyup='javascript:this.value=this.value.toUpperCase();'>
            </div>

        </div>

        <div class="row">
            
            <div class="form-group col-md-6">
                <label>Telefono</label>
                <input type="text" class="form-control" id="telefono_sucursal" name="telefono_sucursal" onKeyPress="return soloNumeros(event);">
            </div>

            <div class="form-group col-md-6">
                <label>Tipo*</label>
                <select class="form-control" id="tipo_sucursal" name="tipo_sucursal">
                    <option value="-1">- SELECCIONAR -</option>
                    <option value="1">MATUTINA</option>
                    <option value="2">TIEMPO COMPLETO</option>
                </select>
            </div>

        </div>

        <div class="row">
            
            <div class="form-group col-md-6">
                <label>Representante</label>
                <input type="text" class="form-control" id="representante_sucursal" name="representante_sucursal" onkeyup='javascript:this.value=this.value.toUpperCase();'>
            </div>

        </div>

    </div>

    

    <div class="box-footer">
        <button type="submit" class="btn btn-primary" id="btnGuardar">Guardar</button>
        <button class="btn btn-primary" id="btnCancelar">Cancelar</button>
    </div>

    <input type="hidden" id="cve_sucursal" name="cve_sucursal">
</form>

<script type="text/javascript">


    $('#formSucursal').on('submit',function(e){
        e.preventDefault();

        if ( $('#nombre_sucursal').val()  == "" )
        {
            msgAlert2("Favor de ingresar el nombre de la sucursal.","warning");
            setTimeout(function() { $("#msgAlert2").fadeOut(1500); },3000);
        }
        else if ( $('#calle_sucursal').val() == "" )
        {
            msgAlert2("Favor de ingresar la dirección de la sucursal.","warning");
            setTimeout(function() { $("#msgAlert2").fadeOut(1500); },3000);
        }
        else if ( $('#colonia_sucursal').val() == "" )
        {
            msgAlert2("Favor de ingresar la colonia de la sucursal.","warning");
            setTimeout(function() { $("#msgAlert2").fadeOut(1500); },3000);
        }
        else if ( $('#tipo_sucursal').val() == "" )
        {
            msgAlert2("Favor de seleccionar el Tipo.","warning");
            setTimeout(function() { $("#msgAlert2").fadeOut(1500); },3000);
        }
        else
        {

            $("#btnGuardar").prop('disabled', true);
            
            $.ajax({
                url      : $(this).attr('action'),
                data     : $(this).serialize(),
                type: "POST",
                success: function(datos){

                    var myJson = JSON.parse(datos);
                    
                    if(myJson.status == "success")
                    {
                        $('#modal_formSucursal').modal('hide');

                        //var table = $('#gridSucursal').DataTable();
                                    
                        //table.clear();
                        //table.destroy();
                        
                        //Reinicializamos tabla
                        cargarTablaSucursal();

                        msgAlert(myJson.msg ,"success");
                        setTimeout(function() { $("#msgAlert").fadeOut(1500); },3000);

                        //$('#msgAlert').css("display", "none");
                        $("#btnGuardar").prop('disabled', false);
                        $("#btnGuardar").html('Guardar');

                    }
                    else
                    {
                        $("#btnGuardar").prop('disabled', false);
                        msgAlert2(myJson.msg ,"danger");
                    }
                }
            }); 
        }
    });

    
    $('#btnCancelar').click(function (e) {
    
        $('#modal_formSucursal').modal('hide');

        return false;
    });


    function msgAlert2(msg,tipo)
    {
        $('#msgAlert2').css("display", "block");
        $("#msgAlert2").html("<div class='alert alert-" + tipo + "' role='alert'>" + msg + " <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button> </div>");
    }

</script>