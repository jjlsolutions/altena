<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo NOMBRE_SITIO; ?> | Home</title>
    <?php 
    include RUTA_APP . 'vistas/includes/link.php'; 
    ?>
</head>
<style>
		.btn-app {
            border-radius: 3px;
            position: relative;
            padding: 15px 5px;
            margin: 0 0 10px 10px;
            min-width: 80px;
            height: 100px;
            text-align: center;
            color: #ffffff;
            border: 1px solid #ddd;
            background-color: #367fa9;
            font-size: 24px;
        }

	</style>
<body class="hold-transition skin-blue sidebar-mini" onload="DoFullScreen()">
<div class="wrapper">

    <?php 
    include RUTA_APP . 'vistas/includes/header.php';

    include RUTA_APP . 'vistas/includes/left_sidebar_menu.php'; 
    ?>


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                
                <small></small>
            </h1>
            
        </section>

        <!-- Main content -->
        <section class="content">
        <div class="container-fluid">
            <div class="row">
            <div class="col-12">
                <!-- /.card-header -->
                <div class="card-body">
                <button onclick="generarVenta(1)" type="button" class="btn btn-app btn-block"><i class="glyphicon glyphicon-star"></i>VENTA</button>
                <button onclick="generarVenta(2)" type="button" class="btn btn-app btn-block"><i class="glyphicon glyphicon-phone-alt"></i>DOMICILIO</button>
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
        </section>
        <!-- /.content -->


    </div>
    <!-- /.content-wrapper -->
    
    <?php 
    //include RUTA_APP . 'vistas/includes/footer.php';

    include RUTA_APP . 'vistas/includes/control_sidebar_right.php';
    ?>

</div>
<!-- ./wrapper -->
<?php 
include RUTA_APP . 'vistas/includes/script.php'; 
?>

<script type="text/javascript">

    function generarVenta(valor){
        if(valor == 1){
            window.location.href = 'venta';
        }
        else if(valor == 2){
            window.location.href = 'domicilio';
        }
       
    }
</script>

</body>
</html>
