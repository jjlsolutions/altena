<!-- Google Font: Source Sans Pro -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
<!-- Font Awesome -->
<link rel="stylesheet" href="<?php echo RUTA_URL; ?>public/plugins/fontawesome-free/css/all.min.css">
<!-- Ion Slider -->
<link rel="stylesheet" href="<?php echo RUTA_URL; ?>public/plugins/ion-rangeslider/css/ion.rangeSlider.min.css">
<!-- bootstrap slider -->
<link rel="stylesheet" href="<?php echo RUTA_URL; ?>public/plugins/bootstrap-slider/css/bootstrap-slider.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="<?php echo RUTA_URL; ?>public/dist/css/adminlte.min.css">
<!-- DataTables -->
<link rel="stylesheet" href="<?php echo RUTA_URL; ?>public/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="<?php echo RUTA_URL; ?>public/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" href="<?php echo RUTA_URL; ?>public/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
<!-- jsGrid -->
<link rel="stylesheet" href="<?php echo RUTA_URL; ?>public/plugins/jsgrid/jsgrid.min.css">
<link rel="stylesheet" href="<?php echo RUTA_URL; ?>public/plugins/jsgrid/jsgrid-theme.min.css">
<!-- SweetAlert2 -->
<link rel="stylesheet" href="<?php echo RUTA_URL; ?>public/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
<!-- Toastr -->
<link rel="stylesheet" href="<?php echo RUTA_URL; ?>public/plugins/toastr/toastr.min.css">
<!-- icheck bootstrap -->
<link rel="stylesheet" href="<?php echo RUTA_URL; ?>public/plugins/icheck-bootstrap/icheck-bootstrap.min.css">