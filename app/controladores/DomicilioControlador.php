<?php
session_start();

if ($_SESSION["cve_usuario"] == "")
{
	header("Location:Login");
}
else
{

	//Heredamos Controlador para poder tener acceso al método modelo y método vista
	class Domicilio extends Controlador
	{
		
		public function __construct()
		{

			$this->domicilioModelo = $this->modelo('DomicilioModelo');

		}



		//Todo controlador debe tener un metodo index
		public function index()
		{
			$this->vista('domicilio/Domicilio');
		}



		public function consultar()
		{
			$data = $this->domicilioModelo->consultar($_POST);

			$envioDatos["arrayDatos"] = $data;

			echo json_encode($envioDatos);
		}



		public function formDomicilio()
		{

			$this->vista('domicilio/formDomicilio', $datos);
		}



		public function guardarDomicilio()
		{
			$datosCompletos = $this->validarDatosVaciosDomicilioGuardar($_POST);

			if ($datosCompletos == "vacio")
			{
				$status = "error";
				$msg = "Favor de revisar el formulario, hay campos requeridos vacios.";
			}
			else
			{
				//Preparamos en un array los datos que enviaremos a la BD
				$cve_domicilio= (int) (!empty($_POST['cve_domicilio']) && $_POST['cve_domicilio']!=null) ? $_POST['cve_domicilio']:'0';

				$datosDomicilio =  array (
									ban                    => 1,
									nombre_domicilio         => $_POST["nombre_domicilio"],
									cveproducto_domicilio      => $_POST["cveproducto_domicilio"],
									clave_domicilio        => $_POST["clave_domicilio"],
									cve_domicilio           => $cve_domicilio,
							     	cveusuario_accion      => $_SESSION["cve_usuario"]
							     );
				
				$respuesta = $this->domicilioModelo->guardarDomicilio($datosDomicilio);

				
				if ($respuesta == true)
				{
					$msg = "Domicilio guardado con Éxito.";
					$status = "success";
				}
				else
				{
					$msg = "Hubo un error al guardar el registro.";
					$status = "error";
				}
				
			}

			
			$envioDatos["status"] = $status;
			$envioDatos["msg"] = $msg;
			echo json_encode($envioDatos);
			
		}



		public function validarDatosVaciosDomicilioGuardar($dataPost)
		{
			if(empty($dataPost["nombre_domicilio"]) || !trim($dataPost["nombre_domicilio"])){ $status = "vacio"; }
			elseif(empty($dataPost["cveproducto_domicilio"]) || !trim($dataPost["cveproducto_domicilio"])){ $status = "vacio"; }
			elseif(empty($dataPost["clave_domicilio"]) || !trim($dataPost["clave_domicilio"])){ $status = "vacio"; }
			else{
				$status = "completo";
			}

			return $status;
		}



		public function bloquearDomicilio()
		{
			$datosDomicilio =  array (
								ban                 => $_POST["ban"],
								cve_domicilio         => $_POST["cve_domicilio"],
								cveusuario_accion   => $_SESSION["cve_usuario"]
						     );

			$respuesta = $this->domicilioModelo->bloquearDomicilio($datosDomicilio);

			if ($respuesta == true)
			{
				if ($datosDomicilio['ban'] == 2)
				{
					$msg = "Domicilio bloqueado.";
				}else{
					$msg = "Domicilio desbloqueado.";
				}
				$status = "success";
			}
			else
			{
				//Este error se presenta por un error en el query
				$msg = "Hubo un error al bloquear el registro.";
				$status = "error";
			}

			$envioDatos["status"] = $status;
			$envioDatos["msg"] = $msg;
			echo json_encode($envioDatos);
		}
		
	}

}


?>